# Project Lamda

## Haszn�lat!!

Ind�tsd el a `start-java-webservice.cmd` parancsf�jlt miel�tt futtatn�d a C# programot.

### Sima TAsk

Adjunk be egy �j taskot:

`lamda task add hello jjs "return 'hell� ' + value;"`

H�vjuk meg ezt a taskot:

`lamda run hello vil�g`

-> "hell� vil�g"


### Pipeline

Sz�ks�ges az el�z� l�p�s teljes�t�se. Majd:

Adjunk be m�gegy taskot:

`lamda task add excl jjs "return value + '!';"`

F�zz�k ezt a k�t taszkot pipelineba:

`lamda pipeline add udvozol hello excl`

H�vjuk meg ezt a taszkot:

`lamda run udvozol vil�g`

-> "hell� vil�g!"

