# LamdaWebService

Webservice endpointot biztosít, mely kaphat bizonyos JavaScript függvényeket, amelyeket le kell futtatni egy kiinduló adathalmazon.
A futtatás eredményét adja vissza egy JSON objektumban.

## Funkciólista

 - Egy bemenő értékre és egy függvénylistára kiad egy kiszámított (transzformált) értéket.

## Példa bemenő adatra

A `http://localhost:8080/lamda/exec` endpointra `POST` metódussal az alábbi JSON dokumentumot küldjük.

```json
{
    "value": [1, 2, 3],
    "lamdas": [{
        "name": "kecske",
        "body": "return 'hello ' + (value.join(','));",
        "lang": "jjs"
    }]
}
```

Erre a szerver válasza valami hasonló:
```json
{
    "lamdas": [{
        "name": "kecske",
        "startedAt": "2019-11-30T20:13:26.989+0000",
        "runtime": 341,
        "result": "hello 1,2,3"
    }],
    "result": "hello 1,2,3"
}
```
