package oenik.LamdaWebService.Executor;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import oenik.LamdaWebService.Model.Lamda;
import oenik.LamdaWebService.Model.LamdaResult;
import oenik.LamdaWebService.Utils.Convert;

public class jjsExecutor {

    public static LamdaResult exec(Lamda lamda, Object input) {
        Date start = new Date();
        try {
            ScriptEngineManager engineManager = new ScriptEngineManager();
            ScriptEngine engine = engineManager.getEngineByName("nashorn");

            LamdaResult r = new LamdaResult();
            r.setName(lamda.getName());
            r.setStartedAt(start);
            
            String expression = "(function (value) { " + lamda.getBody() + " })"
                    + "(" + Convert.toJSCompatible(input) + ")";
            
            Object res = engine.eval(expression);
            r.setResult(res);
            
            r.setRuntime((new Date()).getTime() - start.getTime());
            
            return r;
            
        } catch (ScriptException ex) {
            Logger.getLogger(jjsExecutor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        LamdaResult r = new LamdaResult();
        r.setName(lamda.getName());
        r.setStartedAt(start);
        r.setRuntime(0);
        r.setResult(null);
        return r;
    }
}
