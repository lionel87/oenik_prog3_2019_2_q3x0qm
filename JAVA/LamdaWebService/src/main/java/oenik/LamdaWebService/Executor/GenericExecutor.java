package oenik.LamdaWebService.Executor;

import java.util.Date;
import oenik.LamdaWebService.Model.Lamda;
import oenik.LamdaWebService.Model.LamdaResult;

public class GenericExecutor {

    public static LamdaResult exec(Lamda lamda, Object input) {
        switch (lamda.getLang()) {
            case "jjs":
                return jjsExecutor.exec(lamda, input);
            default:
                LamdaResult lr = new LamdaResult();
                lr.setName(lamda.getName());
                lr.setRuntime(0);
                lr.setStartedAt(new Date(0));
                lr.setResult(null);

                return lr;
        }
    }
}
