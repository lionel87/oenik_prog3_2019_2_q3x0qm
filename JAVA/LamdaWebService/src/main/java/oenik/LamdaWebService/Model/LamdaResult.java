package oenik.LamdaWebService.Model;

import java.util.Date;
import lombok.Data;

@Data
public class LamdaResult {

    private String name;
    private Date startedAt;
    private long runtime;
    private Object result;
}
