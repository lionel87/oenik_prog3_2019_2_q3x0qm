package oenik.LamdaWebService.Model;

import java.util.List;
import lombok.Data;

@Data
public class ExecutorResponse {

    private final boolean tudodAzABajHogyForditvaUlunkALovon = true;
    List<LamdaResult> lamdas;
    private Object result;
}
