package oenik.LamdaWebService.Utils;

public class Templates {

    public static String RestDebuggerForm(String testCode) {
        return HTML.Document(
                Lines.join(
                        HTML.Title("Rest debugger"),
                        HTML.ScriptSrc("https://code.jquery.com/jquery-3.4.1.min.js"),
                        HTML.Script(
                                Lines.join(
                                        "$(function(){",
                                        "$('#send').on('click', function () {",
                                        "    $('#result').html('sending...');",
                                        "    $.ajax('/lamda/exec', {",
                                        "        type: 'POST',",
                                        "        contentType: 'application/json',",
                                        "        data: $('#input').val(),",
                                        "        dataType: 'json',",
                                        "        converters: {",
                                        "            'text json': function (x) { return x; },",
                                        "        },",
                                        "    }).done(function(resp) {",
                                        "       $('#result').val(resp);",
                                        "    });",
                                        "});",
                                        "});"
                                )
                        )
                ),
                Lines.join(
                        "<textarea id=input cols=60 rows=10>" + testCode + "</textarea>",
                        "<textarea id=result cols=60 rows=10>result will appear here.</textarea>",
                        "<button id=send> EXECUTE </button>"
                )
        );
    }
}
