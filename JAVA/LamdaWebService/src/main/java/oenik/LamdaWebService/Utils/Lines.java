package oenik.LamdaWebService.Utils;

public class Lines {
    
    public static String join(String... args) {
        return String.join(System.getProperty("line.separator"), args);
    }
    
}
