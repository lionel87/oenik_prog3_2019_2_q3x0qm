package oenik.LamdaWebService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LamdaWebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LamdaWebServiceApplication.class, args);
	}

}
