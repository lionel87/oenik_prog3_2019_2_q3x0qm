﻿// <copyright file="PipeOwnedLamdas.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Data
{
    using System.Collections.Generic;
    using ProjectLamda.Data;

    /// <summary>
    /// Container for pipeownedlamdas result.
    /// </summary>
    public class PipeOwnedLamdas
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PipeOwnedLamdas"/> class.
        /// </summary>
        /// <param name="pipeline">The pipeline.</param>
        /// <param name="lamdas">Tle list of lamdas.</param>
        public PipeOwnedLamdas(string pipeline, List<lamda> lamdas)
        {
            this.Lamdas = lamdas;
            this.Pipeline = pipeline;
        }

        /// <summary>
        /// Gets the lamdas for this pipe.
        /// </summary>
        public List<lamda> Lamdas { get; }

        /// <summary>
        /// Gets the pipeline name for this.
        /// </summary>
        public string Pipeline { get; }
    }
}