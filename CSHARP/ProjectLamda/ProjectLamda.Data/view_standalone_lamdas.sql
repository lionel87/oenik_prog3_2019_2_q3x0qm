﻿CREATE VIEW standalone_lamdas AS
    SELECT
        l.*
    FROM
        lamdas l
    WHERE
        NOT EXISTS (
            SELECT
                *
            FROM
                lamdas_pipelines pl
            WHERE
                pl.lamda_name = l.name
        );