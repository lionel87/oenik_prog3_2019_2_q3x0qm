﻿--DROP VIEW all_runnables;
--DROP VIEW avg_runtimes;
--DROP VIEW standalone_lamdas;
--DROP TABLE lamdas_pipelines;
--DROP TABLE runtime_stats;
--DROP SEQUENCE runtime_stats_seq;
--DROP TABLE pipelines;
--DROP TABLE lamdas;
--DROP TABLE langs;

CREATE TABLE langs (
    id             VARCHAR(10) NOT NULL,
    display_name   VARCHAR(90) NOT NULL UNIQUE,
    created_at     DATE DEFAULT SYSDATETIME() NOT NULL,
    updated_at     DATE DEFAULT SYSDATETIME() NOT NULL,
    x_van_ennek_ertelme INT DEFAULT 0,
    CONSTRAINT langs_id_pk PRIMARY KEY ( id )
);

CREATE TABLE lamdas (
    name      VARCHAR(255) NOT NULL,
    body      VARCHAR(255) NOT NULL,
    lang_id   VARCHAR(10) NOT NULL,
    created_at     DATE DEFAULT SYSDATETIME() NOT NULL,
    updated_at     DATE DEFAULT SYSDATETIME() NOT NULL,
    CONSTRAINT lamdas_name_pk PRIMARY KEY ( name ),
    CONSTRAINT lamdas_lang_id_fk FOREIGN KEY ( lang_id )
        REFERENCES langs ( id )
);

CREATE TABLE pipelines (
    name VARCHAR(255) NOT NULL,
    created_at     DATE DEFAULT SYSDATETIME() NOT NULL,
    updated_at     DATE DEFAULT SYSDATETIME() NOT NULL,
    x_random_mezoket_veszek_fel_latod INT DEFAULT 1,
    x_olyan_helyekre_ahova_nem_lenne_szabad INT DEFAULT 2,
    CONSTRAINT pipelines_name_pk PRIMARY KEY ( name )
);

CREATE TABLE lamdas_pipelines (
    lamda_name      VARCHAR(255) NOT NULL,
    pipeline_name   VARCHAR(255) NOT NULL,
    position        INTEGER NOT NULL,
    created_at     DATE DEFAULT SYSDATETIME() NOT NULL,
    updated_at     DATE DEFAULT SYSDATETIME() NOT NULL,
    CONSTRAINT lamdas_pipelines_lamda_name_pipeline_name_pk PRIMARY KEY ( lamda_name,
                                                                          pipeline_name ),
    CONSTRAINT lamdas_pipelines_lamda_name_fk FOREIGN KEY ( lamda_name )
        REFERENCES lamdas ( name ),
    CONSTRAINT lamdas_pipelines_pipeline_name_fk FOREIGN KEY ( pipeline_name )
        REFERENCES pipelines ( name )
);

CREATE TABLE runtime_stats (
    id        INTEGER NOT NULL IDENTITY(1, 1),
    name      VARCHAR(255) NOT NULL,
    ran_at    DATE DEFAULT ( SYSDATETIME() ) NOT NULL,
    runtime   INTEGER NOT NULL,
    x_kedves_elemer_ha_ezt_el_tudod_nekem_mondani_miert_jo_megkoszonom INT DEFAULT 1,
    CONSTRAINT runtime_stats_id_fk PRIMARY KEY ( id ),
    CONSTRAINT runtime_stats_name_fk FOREIGN KEY ( name )
        REFERENCES lamdas ( name ),
    CONSTRAINT runtime_stats_no_negative_ck CHECK ( runtime >= 0 )
);
