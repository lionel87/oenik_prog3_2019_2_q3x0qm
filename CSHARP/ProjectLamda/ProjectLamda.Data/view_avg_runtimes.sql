﻿CREATE VIEW avg_runtimes AS
    SELECT
        name,
        AVG(runtime) avg_runtime
    FROM
        runtime_stats
    GROUP BY
        name;

