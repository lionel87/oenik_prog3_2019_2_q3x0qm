﻿// <copyright file="LamdaRunResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic
{
    using System.Collections.Generic;
    using ProjectLamda.Data;

    /// <summary>
    /// Contains lamda runtime information.
    /// </summary>
    public class LamdaRunResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LamdaRunResult"/> class.
        /// </summary>
        /// <param name="a">Result.</param>
        /// <param name="b">Lamdas list.</param>
        public LamdaRunResult(object a, List<LamdaRuntimeStat> b)
        {
            this.Result = a;
            this.Lamdas = b;
        }

        /// <summary>
        /// Gets or sets the result of the computation.
        /// </summary>
        public object Result { get; set; }

        /// <summary>
        /// Gets statistics of a run session.
        /// </summary>
        public List<LamdaRuntimeStat> Lamdas { get; }
    }
}
