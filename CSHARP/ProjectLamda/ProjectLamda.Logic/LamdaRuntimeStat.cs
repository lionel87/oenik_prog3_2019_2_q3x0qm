﻿// <copyright file="LamdaRuntimeStat.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic
{
    using System.Collections.Generic;
    using ProjectLamda.Data;

    /// <summary>
    /// A jvava endpoint majd ebben a formában válaszol az internal lamdas propertyben.
    /// </summary>
    public class LamdaRuntimeStat
    {
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets StartedAt.
        /// </summary>
        public string StartedAt { get; set; }

        /// <summary>
        /// Gets or sets Runtime.
        /// </summary>
        public int Runtime { get; set; }

        /// <summary>
        /// Gets or sets Result.
        /// </summary>
        public object Result { get; set; }
    }
}
