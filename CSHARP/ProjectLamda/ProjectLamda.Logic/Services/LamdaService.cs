﻿// <copyright file="LamdaService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository;
    using ProjectLamda.Repository.Interfaces;

    /// <summary>
    /// Service implementation of the corresponding repository.
    /// </summary>
    public class LamdaService : GenericService<ILamdaRepository, lamda>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LamdaService"/> class.
        /// </summary>
        /// <param name="repository">Repository instance.</param>
        public LamdaService(ILamdaRepository repository)
            : base(repository)
        {
        }

        /// <summary>
        /// Gets all Lamdas whose are part of any pipe.
        /// </summary>
        public List<lamda> AllPipedLamdas
        {
            get
            {
                return this.Repository.AllPipedLamdas.ToList();
            }
        }

        /// <summary>
        /// Gets a specific item by id.
        /// </summary>
        /// <param name="name">Item id.</param>
        /// <returns>Item referenced by given id.</returns>
        public lamda GetById(string name)
        {
            return this.Repository.GetById(name).FirstOrDefault();
        }
    }
}
