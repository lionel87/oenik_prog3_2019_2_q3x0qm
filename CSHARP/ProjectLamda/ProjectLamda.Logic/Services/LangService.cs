﻿// <copyright file="LangService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository;
    using ProjectLamda.Repository.Interfaces;

    /// <summary>
    /// Service implementation of the corresponding repository.
    /// </summary>
    public class LangService : GenericService<ILangRepository, lang>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LangService"/> class.
        /// </summary>
        /// <param name="repository">Repository instance.</param>
        public LangService(ILangRepository repository)
            : base(repository)
        {
        }

        /// <summary>
        /// Gets a specific item by id.
        /// </summary>
        /// <param name="id">Item id.</param>
        /// <returns>Item referenced by given id.</returns>
        public lang GetById(string id)
        {
            return this.Repository.GetById(id).First();
        }
    }
}
