﻿// <copyright file="StandaloneLamdasService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository;

    /// <summary>
    /// Service implementation of the corresponding repository.
    /// </summary>
    public class StandaloneLamdasService : GenericService<StandaloneLamdasRepository, standalone_lamdas>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StandaloneLamdasService"/> class.
        /// </summary>
        /// <param name="repository">Repository instance.</param>
        public StandaloneLamdasService(StandaloneLamdasRepository repository)
            : base(repository)
        {
        }
    }
}
