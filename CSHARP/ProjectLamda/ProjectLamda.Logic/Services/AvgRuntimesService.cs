﻿// <copyright file="AvgRuntimesService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository;

    /// <summary>
    /// Service implementation of the corresponding repository.
    /// </summary>
    public class AvgRuntimesService : GenericService<AvgRuntimesRepository, avg_runtimes>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AvgRuntimesService"/> class.
        /// </summary>
        /// <param name="repository">Repository instance.</param>
        public AvgRuntimesService(AvgRuntimesRepository repository)
            : base(repository)
        {
        }
    }
}
