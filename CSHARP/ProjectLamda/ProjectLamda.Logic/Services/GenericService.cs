﻿// <copyright file="GenericService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using ProjectLamda.Logic.Interfaces;
    using ProjectLamda.Repository.Interfaces;

    /// <summary>
    /// Generic CRUD service implementation.
    /// </summary>
    /// <typeparam name="TRepository">Repository type.</typeparam>
    /// <typeparam name="TEntity">Entity type.</typeparam>
    public class GenericService<TRepository, TEntity> : IGenericService<TEntity>
        where TRepository : IGenericRepository<TEntity>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericService{TRepository, TEntity}"/> class.
        /// </summary>
        /// <param name="repository">Repository instance to initialize with.</param>
        public GenericService(TRepository repository)
        {
            this.Repository = repository;
        }

        /// <summary>
        /// Gets the repository of the current service.
        /// </summary>
        protected TRepository Repository { get; private set; }

        /// <inheritdoc/>
        public List<TEntity> SelectAll()
        {
            return this.Repository.SelectAll().ToList();
        }

        /// <inheritdoc/>
        public void Insert(TEntity item)
        {
            this.Repository.Insert(item);
        }

        /// <inheritdoc/>
        public void Update(TEntity item)
        {
            this.Repository.Update(item);
        }

        /// <inheritdoc/>
        public void Delete(TEntity item)
        {
            this.Repository.Delete(item);
        }
    }
}
