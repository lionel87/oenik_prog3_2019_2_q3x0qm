﻿// <copyright file="LamdaPipelineService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository;
    using ProjectLamda.Repository.Interfaces;

    /// <summary>
    /// Service implementation of the corresponding repository.
    /// </summary>
    public class LamdaPipelineService : GenericService<IPipelineLamdaRepository, lamdas_pipelines>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LamdaPipelineService"/> class.
        /// </summary>
        /// <param name="repository">Repository instance.</param>
        public LamdaPipelineService(IPipelineLamdaRepository repository)
            : base(repository)
        {
        }

        /// <summary>
        /// Gets a specific item by id.
        /// </summary>
        /// <param name="pipe">The pipeline name.</param>
        /// <param name="lamda">The lamda name.</param>
        /// <returns>Item referenced by given id.</returns>
        public lamdas_pipelines GetById(string pipe, string lamda)
        {
            return this.Repository.GetById(pipe, lamda).First();
        }

        /// <summary>
        /// Gets all records where the given pipeline is the pipeline.
        /// </summary>
        /// <param name="pip">The pipeline to search for.</param>
        /// <returns>List of joiners.</returns>
        public List<lamdas_pipelines> GetByPipeline(pipeline pip)
        {
            return this.Repository.SelectAll().Where(x => x.pipeline_name == pip.name).ToList();
        }

        /// <summary>
        /// Gets all records where the given lamda is the lamda.
        /// </summary>
        /// <param name="lam">The lamda to search for.</param>
        /// <returns>List of joiners.</returns>
        public List<lamdas_pipelines> GetByLamda(lamda lam)
        {
            return this.Repository.SelectAll().Where(x => x.lamda_name == lam.name).ToList();
        }
    }
}
