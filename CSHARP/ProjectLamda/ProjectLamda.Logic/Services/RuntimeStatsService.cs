﻿// <copyright file="RuntimeStatsService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository.Interfaces;

    /// <summary>
    /// Service implementation of the corresponding repository.
    /// </summary>
    public class RuntimeStatsService : GenericService<IRuntimeStatsRepository, runtime_stats>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RuntimeStatsService"/> class.
        /// </summary>
        /// <param name="repository">Repository instance.</param>
        public RuntimeStatsService(IRuntimeStatsRepository repository)
            : base(repository)
        {
        }

        /// <summary>
        /// Gets the runtime averages.
        /// </summary>
        public List<runtime_stats> GetRuntimeAverages
        {
            get
            {
                return (from rt in this.Repository.SelectAll()
                       group rt by rt.name into g
                       select new runtime_stats()
                       {
                           name = g.Key,
                           runtime = Convert.ToInt32(g.Average(p => p.runtime)),
                       })
                       .ToList();
            }
        }

        /// <summary>
        /// Stores a runtime result provided by LamdaRunnerService.
        /// </summary>
        /// <param name="runs">List of runned lamdas.</param>
        public void StoreFromRuntimeResult(List<LamdaRuntimeStat> runs)
        {
            if (runs == null)
            {
                throw new ArgumentNullException(nameof(runs));
            }

            runs.ForEach(x => this.Insert(new runtime_stats()
            {
                name = x.Name,
                ran_at = DateTime.Parse(x.StartedAt, null, System.Globalization.DateTimeStyles.RoundtripKind),
                runtime = x.Runtime,
            }));
        }
    }
}
