﻿// <copyright file="ComplexTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using ProjectLamda.Data;
    using ProjectLamda.Repository.Interfaces;

    /// <summary>
    /// LamdaTests.
    /// </summary>
    [TestFixture]
    public class ComplexTests
    {
        /// <summary>
        /// Testing select all CRUD.
        /// </summary>
        [Test]
        public void AllPipedLamdasTest()
        {
            // Arrange
            IList<lamda> lamdas = new List<lamda>()
            {
                new lamda()
                {
                    name = "hello",
                    lang_id = "jjs",
                    body = "return 'hello ' + value;",
                },
            };

            Mock<ILamdaRepository> repo = new Mock<ILamdaRepository>();
            repo.Setup(mr => mr.AllPipedLamdas).Returns(lamdas.AsQueryable());
            LamdaService svc = new LamdaService(repo.Object);

            // Act
            var result = svc.AllPipedLamdas;

            // Assert
            Assert.AreEqual(result.Count, 1);
        }

        /// <summary>
        /// Testing insert into.
        /// </summary>
        [Test]
        public void GetPipeOwnedLamdasTest()
        {
            IList<PipeOwnedLamdas> pl = new List<PipeOwnedLamdas>()
            {
                new PipeOwnedLamdas("hello-pipe", new List<lamda>()
                {
                    new lamda()
                    {
                        name = "hello",
                        lang_id = "jjs",
                        body = "return 'hello ' + value;",
                    },
                }),
            };

            Mock<IPipelineRepository> repo = new Mock<IPipelineRepository>();
            repo.Setup(mr => mr.GetPipeOwnedLamdas).Returns(pl.AsQueryable());
            PipelineService svc = new PipelineService(repo.Object);

            Assert.AreEqual(1, svc.GetPipeOwnedLamdas.Count);
        }

        /// <summary>
        /// Testing update.
        /// </summary>
        [Test]
        public void GetRuntimeAverages()
        {
            IList<runtime_stats> runtimes = new List<runtime_stats>()
            {
                new runtime_stats()
                {
                    name = "hello",
                    ran_at = default,
                    runtime = 0,
                },
                new runtime_stats()
                {
                    name = "hello",
                    ran_at = default,
                    runtime = 10,
                },
            };

            Mock<IRuntimeStatsRepository> repo = new Mock<IRuntimeStatsRepository>();
            repo.Setup(mr => mr.SelectAll()).Returns(runtimes.AsQueryable());
            RuntimeStatsService svc = new RuntimeStatsService(repo.Object);

            Assert.AreEqual(5, svc.GetRuntimeAverages.First().runtime);
        }
    }
}
