﻿using Newtonsoft.Json;
using ProjectLamda.Data;
using ProjectLamda.Logic;
using ProjectLamda.Repository;
using ProjectLamda.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjectLamda.WebApp.Controllers
{
    [Route("api/task")]
    public class LamdaTaskApiController : ApiController
    {
        private readonly LamdaService TS = new LamdaService(new LamdaRepository());

        // GET api/tasks
        public IEnumerable<TaskModel> Get()
        {
            return TS.SelectAll().Select(x => new TaskModel(x));
        }

        // GET api/tasks/5
        public TaskModel Get(string id)
        {
            return new TaskModel(TS.GetById(id));
        }

        // POST api/tasks
        public void Post([FromBody]TaskModel value)
        {
            var task = new lamda()
            {
                name = value.Name,
                body = value.Body,
                lang_id = value.LangId,
                created_at = DateTime.UtcNow,
                updated_at = DateTime.UtcNow,
            };

            TS.Insert(task);
        }

        // PUT api/tasks/5
        public void Put(string id, [FromBody]TaskModel value)
        {
            var task = TS.GetById(id);
            task.body = value.Body;
            task.lang_id = value.LangId;
            task.updated_at = DateTime.UtcNow;

            TS.Update(task);
        }

        // DELETE api/tasks/5
        public void Delete(string id)
        {
            TS.Delete(TS.GetById(id));
        }
    }
}
