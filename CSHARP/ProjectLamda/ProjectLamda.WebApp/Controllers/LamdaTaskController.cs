﻿using ProjectLamda.Data;
using ProjectLamda.Logic;
using ProjectLamda.Repository;
using ProjectLamda.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectLamda.WebApp.Controllers
{
    public class LamdaTaskController : Controller
    {
        private LamdaService TS = new LamdaService(new LamdaRepository());
        private LangService LS = new LangService(new LangRepository());

        // GET: LamdaTask
        public ActionResult Index()
        {
            return View(TS.SelectAll());
        }

        // GET: LamdaTask/Details/5
        public ActionResult Details(string id)
        {
            return View(TS.GetById(id));
        }

        // GET: LamdaTask/Create
        public ActionResult Create()
        {
            return View(new CreateEditTaskModel()
            {
                Task = new lamda(),
                Langs = LS.SelectAll(),
            });
        }

        // POST: LamdaTask/Create
        [HttpPost]
        public ActionResult Create(CreateEditTaskModel l)
        {
            try
            {
                l.Task.created_at = DateTime.UtcNow;
                l.Task.updated_at = DateTime.UtcNow;

                TS.Insert(l.Task);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LamdaTask/Edit/5
        public ActionResult Edit(string id)
        {
            return View(new CreateEditTaskModel()
            {
                Task = TS.GetById(id),
                Langs = LS.SelectAll(),
            });
        }

        // POST: LamdaTask/Edit/5
        [HttpPost]
        public ActionResult Edit(string id, CreateEditTaskModel l)
        {
            try
            {
                var ol = TS.GetById(id);

                ol.body = l.Task.body;
                ol.lang_id = l.Task.lang_id;

                ol.updated_at = DateTime.UtcNow;

                TS.Update(ol);

                return RedirectToAction("Index");
            }
            catch
            {
                return View(new CreateEditTaskModel()
                {
                    Task = TS.GetById(id),
                    Langs = LS.SelectAll(),
                });
            }
        }

        // GET: LamdaTask/Delete/5
        public ActionResult Delete(string id)
        {
            return View(TS.GetById(id));
        }

        // POST: LamdaTask/Delete/5
        [HttpPost]
        public ActionResult Delete(string id, FormCollection collection)
        {
            try
            {
                TS.Delete(TS.GetById(id));

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
