﻿using ProjectLamda.Data;
using System;

namespace ProjectLamda.WebApp.Models
{
    public class TaskModel
    {
        public string Name { get; set; }
        public string Body { get; set; }
        public string LangId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public TaskModel()
        {
        }

        public TaskModel(lamda l)
        {
            Name = l.name;
            Body = l.body;
            LangId = l.lang_id;
            CreatedAt = l.created_at;
            UpdatedAt = l.updated_at;
        }
    }
}