﻿// <copyright file="LangRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository
{
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository.Interfaces;

    /// <inheritdoc/>
    public class LangRepository : GenericRepository<lang>, ILangRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LangRepository"/> class.
        /// </summary>
        public LangRepository()
            : base(new dbEntities())
        {
        }

        /// <inheritdoc/>
        public IQueryable<lang> GetById(string id)
        {
            return this.SelectAll().Where(x => x.id == id);
        }
    }
}
