﻿// <copyright file="AllRunnablesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository
{
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository.Interfaces;

    /// <inheritdoc/>
    public class AllRunnablesRepository : GenericRepository<all_runnables>, IAllRunnablesRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AllRunnablesRepository"/> class.
        /// </summary>
        public AllRunnablesRepository()
            : base(new dbEntities())
        {
        }

        /// <inheritdoc/>
        public IQueryable<all_runnables> GetById(string name)
        {
            return this.SelectAll().Where(x => x.name == name);
        }
    }
}
