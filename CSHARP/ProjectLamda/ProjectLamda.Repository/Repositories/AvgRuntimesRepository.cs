﻿// <copyright file="AvgRuntimesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository
{
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository.Interfaces;

    /// <inheritdoc/>
    public class AvgRuntimesRepository : GenericRepository<avg_runtimes>, IAvgRuntimesRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AvgRuntimesRepository"/> class.
        /// </summary>
        public AvgRuntimesRepository()
            : base(new dbEntities())
        {
        }
    }
}
