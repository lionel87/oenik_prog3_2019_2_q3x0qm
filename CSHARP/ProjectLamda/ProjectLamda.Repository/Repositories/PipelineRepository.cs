﻿// <copyright file="PipelineRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository
{
    using System.Linq;
    using ProjectLamda.Data;
    using ProjectLamda.Repository.Interfaces;

    /// <inheritdoc/>
    public class PipelineRepository : GenericRepository<pipeline>, IPipelineRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PipelineRepository"/> class.
        /// </summary>
        public PipelineRepository()
            : base(new dbEntities())
        {
        }

        /// <inheritdoc/>
        public IQueryable<PipeOwnedLamdas> GetPipeOwnedLamdas
        {
            get
            {
                using var ctx = new dbEntities();

                return from p in this.SelectAll()
                       join pl in ctx.lamdas_pipelines on p.name equals pl.lamda_name
                       join l in ctx.lamdas on pl.lamda_name equals l.name
                       orderby pl.position ascending
                       group l by p into g
                       select new PipeOwnedLamdas(g.Key.name, g.ToList());
                       /*select new { Pipeline = g.Key.name, Lamdas = g.ToList() };*/
            }
        }

        /// <inheritdoc/>
        public IQueryable<pipeline> GetById(string name)
        {
            return this.SelectAll().Where(x => x.name == name);
        }
    }
}
