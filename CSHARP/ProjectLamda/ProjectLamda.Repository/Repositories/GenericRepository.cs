﻿// <copyright file="GenericRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using ProjectLamda.Repository.Interfaces;

    /// <summary>
    /// Generic repository class to handle basic CRUD operations.
    /// </summary>
    /// <typeparam name="TEntity">Table type in repository.</typeparam>
    public class GenericRepository<TEntity> : IGenericRepository<TEntity>
        where TEntity : class
    {
        private readonly DbSet<TEntity> entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{TEntity}"/> class.
        /// </summary>
        /// <param name="context">Database entities type.</param>
        public GenericRepository(DbContext context)
        {
            this.Context = context ?? throw new ArgumentNullException(nameof(context));
            this.entities = context.Set<TEntity>();
        }

        /// <summary>
        /// Gets the entities context.
        /// </summary>
        protected DbContext Context { get; private set; }

        /// <inheritdoc/>
        public virtual IQueryable<TEntity> SelectAll()
        {
            return this.entities;
        }

        /// <inheritdoc/>
        public virtual void Insert(TEntity item)
        {
            this.entities.Add(item);
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public virtual void Update(TEntity item)
        {
            this.entities.Attach(item);
            this.Context.Entry(item).State = EntityState.Modified;
            this.Context.SaveChanges();
        }

        /// <inheritdoc/>
        public virtual void Delete(TEntity item)
        {
            this.entities.Remove(item);
            this.Context.SaveChanges();
        }
    }
}
