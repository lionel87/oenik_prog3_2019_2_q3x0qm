﻿// <copyright file="IAvgRuntimesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository.Interfaces
{
    using System.Linq;
    using ProjectLamda.Data;

    /// <summary>
    /// Repository class for StandaloneLamdas view.
    /// </summary>
    public interface IAvgRuntimesRepository : IGenericRepository<avg_runtimes>
    {
    }
}
