﻿// <copyright file="ILamdaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository.Interfaces
{
    using System.Linq;
    using ProjectLamda.Data;

    /// <summary>
    /// Repository class for lamdas table.
    /// </summary>
    public interface ILamdaRepository : IGenericRepository<lamda>
    {
        /// <summary>
        /// Gets all lamdas which is part of a pipeline.
        /// </summary>
        public IQueryable<lamda> AllPipedLamdas { get; }

        /// <summary>
        /// Gets a specific item by its id.
        /// </summary>
        /// <param name="name">Item id.</param>
        /// <returns>The desired item.</returns>
        public IQueryable<lamda> GetById(string name);

        /// <summary>
        /// Finds itesm by pipeline.
        /// </summary>
        /// <param name="name">Pipe id/name.</param>
        /// <returns>Lamdas in pipeline.</returns>
        public IQueryable<lamda> GetByPipe(string name);
    }
}
