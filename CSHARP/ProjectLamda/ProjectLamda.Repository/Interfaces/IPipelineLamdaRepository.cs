﻿// <copyright file="IPipelineLamdaRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository.Interfaces
{
    using System.Linq;
    using ProjectLamda.Data;

    /// <summary>
    /// Repository class for lamdas_pipelines table.
    /// </summary>
    public interface IPipelineLamdaRepository : IGenericRepository<lamdas_pipelines>
    {
        /// <summary>
        /// Gets a specific item by its id.
        /// </summary>
        /// <param name="pipe">Pipelien name.</param>
        /// <param name="lamda">Lamda name.</param>
        /// <returns>The desired item.</returns>
        public IQueryable<lamdas_pipelines> GetById(string pipe, string lamda);
    }
}
