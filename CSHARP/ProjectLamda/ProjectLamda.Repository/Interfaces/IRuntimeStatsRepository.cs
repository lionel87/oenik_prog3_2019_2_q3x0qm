﻿// <copyright file="IRuntimeStatsRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository.Interfaces
{
    using System.Linq;
    using ProjectLamda.Data;

    /// <summary>
    /// Repository class for RuntimeStats table.
    /// </summary>
    public interface IRuntimeStatsRepository : IGenericRepository<runtime_stats>
    {
        /// <summary>
        /// Gets a specific item by its id.
        /// </summary>
        /// <param name="id">Item id.</param>
        /// <returns>The desired item.</returns>
        public IQueryable<runtime_stats> GetById(int id);
    }
}
