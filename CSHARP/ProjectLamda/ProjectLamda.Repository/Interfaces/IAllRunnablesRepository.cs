﻿// <copyright file="IAllRunnablesRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository.Interfaces
{
    using System.Linq;
    using ProjectLamda.Data;

    /// <summary>
    /// Repository class for AllRunnables view.
    /// </summary>
    public interface IAllRunnablesRepository : IGenericRepository<all_runnables>
    {
        /// <summary>
        /// Gets a specific item by its id.
        /// </summary>
        /// <param name="name">Item id.</param>
        /// <returns>The desired item.</returns>
        public IQueryable<all_runnables> GetById(string name);
    }
}
