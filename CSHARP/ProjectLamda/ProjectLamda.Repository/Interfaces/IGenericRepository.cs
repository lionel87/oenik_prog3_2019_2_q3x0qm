﻿// <copyright file="IGenericRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda.Repository.Interfaces
{
    using System.Linq;

    /// <summary>
    /// Generic repository interface to handle basic CRUD operations.
    /// </summary>
    /// <typeparam name="TEntity">Item type.</typeparam>
    public interface IGenericRepository<TEntity>
    {
        /// <summary>
        /// Gets all the items from the database as an IQueryable.
        /// </summary>
        /// <returns>All the items.</returns>
        public IQueryable<TEntity> SelectAll();

        /// <summary>
        /// Inserts the item to the database.
        /// </summary>
        /// <param name="item">Item to insert.</param>
        public void Insert(TEntity item);

        /// <summary>
        /// Updates an existing record in the database.
        /// </summary>
        /// <param name="item">Item to update.</param>
        public void Update(TEntity item);

        /// <summary>
        /// Deletes this item from the database.
        /// </summary>
        /// <param name="item">Item to delete.</param>
        public void Delete(TEntity item);
    }
}
