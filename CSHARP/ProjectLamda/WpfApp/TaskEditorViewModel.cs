﻿using GalaSoft.MvvmLight;
using ProjectLamda.Data;
using System.Collections.Generic;

namespace WpfApp
{
    public class TaskEditorViewModel : ViewModelBase
    {
        private bool nameEditable;
        private TaskModel task;

        public TaskModel Task
        {
            get { return task; }
            set { Set(ref task, value); }
        }

        public bool NameEditable
        {
            get { return nameEditable; }
            set { Set(ref nameEditable, value); }
        }
    }
}
