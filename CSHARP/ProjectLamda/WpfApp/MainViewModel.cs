﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp
{
    class MainViewModel : ViewModelBase
    {
        private TheLogic logic;
		private TaskModel selected;
		private ObservableCollection<TaskModel>	tasks;

		public ObservableCollection<TaskModel> Tasks
		{
			get { return tasks; }
			set { Set(ref tasks, value); }
		}

		public TaskModel Selected
		{
			get { return selected; }
			set { Set(ref selected, value); }
		}

		public ICommand ListCmd { get; set; }
		public ICommand AddCmd { get; private set; }
		public ICommand EditCmd { get; private set; }
		public ICommand DelCmd { get; private set; }

		public Func<TaskEditorViewModel, bool> EditorFunct { get; set; }

		public MainViewModel()
		{
			logic = new TheLogic();

			AddCmd = new RelayCommand(() => logic.ShowEditor(null, EditorFunct));
			EditCmd = new RelayCommand(() => logic.ShowEditor(Selected, EditorFunct));
			DelCmd = new RelayCommand(() => logic.DeleteTask(Selected));
			ListCmd = new RelayCommand(() => Tasks = new ObservableCollection<TaskModel>(logic.GetTasks()));
		}
	}
}
