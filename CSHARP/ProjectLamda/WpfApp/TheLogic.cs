﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp
{
    class TheLogic
    {
        static string BaseUrl = "http://localhost:50639/";
        static string ServiceUrl = BaseUrl + "api/task";

        private HttpClient http = new HttpClient();

        void QueryOk(bool succ)
        {
            Messenger.Default.Send(succ ? "Ok" : "Failed", "TaskHTTPResult");
        }

        public List<TaskModel> GetTasks()
        {

            string body = http.GetStringAsync(ServiceUrl).Result;
            var result = JsonConvert.DeserializeObject<List<TaskModel>>(body);
            return result;
        }

        public void DeleteTask(TaskModel task)
        {
            if (task is null)
            {
                throw new ArgumentNullException(nameof(task));
            }

            var result = http.DeleteAsync($"{ServiceUrl}?id={task.Name}").Result;
            QueryOk(result.IsSuccessStatusCode);
        }

        public void EditTask(TaskModel task)
        {
            var content = JsonConvert.SerializeObject(task);
            var response = http.PutAsync($"{ServiceUrl}?id={task.Name}", new StringContent(content, Encoding.UTF8, "application/json")).Result;
            QueryOk(response.IsSuccessStatusCode);
        }

        public void InsertTask(TaskModel task)
        {
            var content = JsonConvert.SerializeObject(task);
            var response = http.PostAsync(ServiceUrl, new StringContent(content, Encoding.UTF8, "application/json")).Result;
            QueryOk(response.IsSuccessStatusCode);
        }

        public void ShowEditor(TaskModel task, Func<TaskEditorViewModel, bool> editor)
        {
            var t = new TaskModel();
            var editing = false;
            if (task != null)
            {
                t.SetFrom(task);
                editing = true;
            }

            var vm = new TaskEditorViewModel()
            {
                Task = t,
                NameEditable = editing,
            };

            bool? su = editor?.Invoke(vm);

            if (su == true)
            {
                if (editing)
                {
                    EditTask(t);
                }
                else
                {
                    InsertTask(t);
                }
            }
        }
    }
}
