﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WEBAPIConsoleClient
{
    class Program
    {
        static string BaseUrl = "http://localhost:50639/";
        static string ServiceUrl = BaseUrl + "api/task";

        static void Main(string[] args)
        {
            Console.WriteLine("Press <ENTER> when webserver loaded.");
            Console.ReadLine();

            using (HttpClient client = new HttpClient())
            {
                ListAll(client);
                AddNew(client);
                ListAll(client);
                Update(client);
                ListAll(client);
                Delete(client);
                ListAll(client);
            }

            Console.ReadLine();
        }

        private static void AddNew(HttpClient client)
        {
            var content = JsonConvert.SerializeObject(new TaskModel()
            {
                Name = "probafn",
                Body = "return 'function body';",
                LangId = "jjs",
            });
            var response = client.PostAsync(ServiceUrl, new StringContent(content, Encoding.UTF8, "application/json")).Result;
            Console.WriteLine(response.IsSuccessStatusCode ? "Add: success" : "Add: failed");
            Console.WriteLine();
        }

        private static void Update(HttpClient client)
        {
            var rnd = new Random();
            var content = JsonConvert.SerializeObject(new TaskModel()
            {
                LangId = "jjs",
                Body = $"return '{rnd.Next(1, 100000)}';",
            });
            var response = client.PutAsync(ServiceUrl + "?id=probafn", new StringContent(content, Encoding.UTF8, "application/json")).Result;
            Console.WriteLine(response.IsSuccessStatusCode ? "Update: success" : "Update: failed");
            Console.WriteLine();
        }

        private static void Delete(HttpClient client)
        {
            var response = client.DeleteAsync(ServiceUrl + "?id=probafn").Result;
            Console.WriteLine(response.IsSuccessStatusCode ? "Delete: success" : "Delete: failed");
            Console.WriteLine();
        }

        private static void ListAll(HttpClient client)
        {
            Console.WriteLine("Listing all tasks.");
            string result = client.GetStringAsync(ServiceUrl).Result;

            var tasks = JsonConvert.DeserializeObject<List<TaskModel>>(result);

            Console.WriteLine("Results:");
            foreach (var task in tasks)
            {
                Console.WriteLine(task);
            }
            Console.WriteLine();
        }
    }
}
