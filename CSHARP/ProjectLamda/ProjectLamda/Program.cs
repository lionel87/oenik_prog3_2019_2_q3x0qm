﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ProjectLamda
{
    using System;
    using System.Collections.Generic;
    using ProjectLamda.Data;
    using ProjectLamda.Logic;
    using ProjectLamda.Repository;

    /// <summary>
    /// Main entry class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// main entry algo.
        /// </summary>
        /// <param name="args">cli params.</param>
        public static void Main(string[] args)
        {
            if (args == null || args.Length < 1)
            {
                System.Console.WriteLine(Properties.Resources.Usage);
                return;
            }

            switch (args[0])
            {
                case "lang":

                    if (args.Length < 2)
                    {
                        System.Console.WriteLine(Properties.Resources.UnknownCommand);
                        return;
                    }

                    LangService ls = new LangService(new LangRepository());

                    switch (args[1])
                    {
                        case "list":
                            ls.SelectAll().ForEach(x => System.Console.WriteLine($"{x.id}: {x.display_name}"));
                            break;
                        case "add":
                            if (args.Length < 4)
                            {
                                System.Console.WriteLine(Properties.Resources.UnknownCommand);
                                return;
                            }

                            ls.Insert(new lang()
                            {
                                id = args[2],
                                display_name = args[3],
                            });

                            Console.WriteLine(Properties.Resources.OK);

                            break;
                        case "edit":
                            if (args.Length < 4)
                            {
                                System.Console.WriteLine(Properties.Resources.UnknownCommand);
                                return;
                            }

                            lang la = ls.GetById(args[2]);

                            la.display_name = args[3];
                            la.updated_at = default;

                            ls.Update(la);

                            Console.WriteLine(Properties.Resources.OK);

                            break;

                        case "delete":
                            if (args.Length < 3)
                            {
                                System.Console.WriteLine(Properties.Resources.UnknownCommand);
                                return;
                            }

                            ls.Delete(ls.GetById(args[2]));

                            Console.WriteLine(Properties.Resources.OK);

                            break;
                        default:
                            System.Console.WriteLine(Properties.Resources.UnknownCommand);
                            break;
                    }

                    break;

                case "task":

                    if (args.Length < 2)
                    {
                        System.Console.WriteLine(Properties.Resources.UnknownCommand);
                        return;
                    }

                    LamdaService ts = new LamdaService(new LamdaRepository());

                    switch (args[1])
                    {
                        case "list":
                            ts.SelectAll().ForEach(x => System.Console.WriteLine(x.name));
                            break;
                        case "add":
                            if (args.Length < 5)
                            {
                                System.Console.WriteLine(Properties.Resources.UnknownCommand);
                                return;
                            }

                            ts.Insert(new lamda()
                            {
                                name = args[2],
                                lang_id = args[3],
                                body = args[4],
                            });

                            Console.WriteLine(Properties.Resources.OK);

                            break;
                        case "edit":
                            if (args.Length < 5)
                            {
                                System.Console.WriteLine(Properties.Resources.UnknownCommand);
                                return;
                            }

                            lamda la = ts.GetById(args[2]);

                            LangService lsForLaEd = new LangService(new LangRepository());

                            la.lang = lsForLaEd.GetById(args[3]);
                            la.body = args[4];
                            la.updated_at = default;

                            ts.Update(la);

                            Console.WriteLine(Properties.Resources.OK);

                            break;

                        case "delete":
                            if (args.Length < 3)
                            {
                                System.Console.WriteLine(Properties.Resources.UnknownCommand);
                                return;
                            }

                            ts.Delete(ts.GetById(args[2]));

                            Console.WriteLine(Properties.Resources.OK);

                            break;
                        default:
                            System.Console.WriteLine(Properties.Resources.UnknownCommand);
                            break;
                    }

                    break;

                case "pipeline":

                    if (args.Length < 2)
                    {
                        System.Console.WriteLine(Properties.Resources.UnknownCommand);
                        return;
                    }

                    PipelineService ps = new PipelineService(new PipelineRepository());

                    switch (args[1])
                    {
                        case "list":
                            ps.SelectAll().ForEach(x => System.Console.WriteLine(x.name));
                            break;
                        case "add":
                            if (args.Length < 5)
                            {
                                System.Console.WriteLine(Properties.Resources.UnknownCommand);
                                return;
                            }

                            pipeline pip = new pipeline()
                            {
                                name = args[2],
                            };

                            ps.Insert(pip);

                            LamdaPipelineService lps = new LamdaPipelineService(new PipelineLamdaRepository());

                            for (int i = 3; i < args.Length; i++)
                            {
                                lps.Insert(new lamdas_pipelines()
                                {
                                    pipeline_name = args[2],
                                    lamda_name = args[i],
                                    position = i,
                                });
                            }

                            Console.WriteLine(Properties.Resources.OK);

                            break;
                        case "edit":
                            if (args.Length < 5)
                            {
                                System.Console.WriteLine(Properties.Resources.UnknownCommand);
                                return;
                            }

                            pipeline pi = ps.GetById(args[2]);
                            LamdaService tshe = new LamdaService(new LamdaRepository());
                            LamdaPipelineService lpse = new LamdaPipelineService(new PipelineLamdaRepository());

                            pi.updated_at = default;

                            ps.Update(pi);

                            // delete all.
                            lpse.GetByPipeline(pi).ForEach(x => lpse.Delete(x));

                            for (int i = 3; i < args.Length; i++)
                            {
                                lpse.Insert(new lamdas_pipelines()
                                {
                                    pipeline = pi,
                                    lamda = tshe.GetById(args[i]),
                                    position = i,
                                });
                            }

                            Console.WriteLine(Properties.Resources.OK);

                            break;

                        case "delete":
                            if (args.Length < 3)
                            {
                                System.Console.WriteLine(Properties.Resources.UnknownCommand);
                                return;
                            }

                            ps.Delete(ps.GetById(args[2]));

                            Console.WriteLine(Properties.Resources.OK);

                            break;
                        default:
                            System.Console.WriteLine(Properties.Resources.UnknownCommand);
                            break;
                    }

                    break;
                case "run":
                    if (args.Length < 3)
                    {
                        System.Console.WriteLine(Properties.Resources.UnknownCommand);
                        return;
                    }

                    LamdaRunResult lrr = LamdaRunnerService.RunThis(args[1], args[2]);
                    RuntimeStatsService rss = new RuntimeStatsService(new RuntimeStatsRepository());

                    rss.StoreFromRuntimeResult(lrr.Lamdas);

                    Console.WriteLine("-> " + lrr.Result);

                    break;

                case "stats":
                    if (args.Length == 1)
                    {
                        System.Console.WriteLine(Properties.Resources.UnknownCommand);
                        return;
                    }
                    else
                    {
                        switch (args[1])
                        {
                            case "1":
                                break;
                            case "2":
                                break;
                            case "3":
                                break;
                            default:
                                System.Console.WriteLine(Properties.Resources.UnknownCommand);
                                break;
                        }
                    }

                    break;
                default:
                    System.Console.WriteLine(Properties.Resources.UnknownCommand);
                    break;
            }
        }
    }
}
