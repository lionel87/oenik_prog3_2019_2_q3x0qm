# ProjectLamda

A feladata hogy serverless környezetet emuláljon. A felhasználó JavaScript kódokat futtat, megadott sorrendben, gyakorlatilag egy pipeline-t definiálva.
A pipeline elindulhat kérésre, vagy egy webes végpontot meghívva.

## Funkciólista

- Nyilvántart egy függvény listát.
- A függvény listából pipeline készíthető.
- A pipeline lefuttatható egy adott értékkel, melyet kiszámol a JAVA webservice.
- A futásról diagnosztika kérdezhető le.

További információk:
[What is Serverless?](https://serverless-stack.com/chapters/what-is-serverless.html)